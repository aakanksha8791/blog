class ArticlesController < ApplicationController
   
    before_action :set_article, only: [:edit, :update, :show, :destroy]
    def index
        @articles = Article.all
    end
 
    def show
        @article = Article.find(params[:id])
    end
 
    def new
        @article = Article.new
    end
 
    def create
        #render plain: params[:article].inspect
        @article = Article.new(article_params)
        @article.user = User.first
        if @article.save
            flash[:success]  = "Article was successfully created"
            redirect_to @article
        else 
            #flash[:success]  =  @article.errors.full_messages 
            render 'new'
        end
    end

    def edit
    end 

    def update
        if @article.update(article_params)
            flash[:success]  = "Article was updated"
         redirect_to @article
        else
         #flash[:notice] = @article.errors.full_messages #"Article was not updated"
         render 'edit'
        end
    end

    def destroy
        @article.destroy
        #flash[:notice] = "Article was deleted"
        redirect_to articles_path
       end
 private
    
   def article_params
        params.require(:article).permit(:name, :description)
    end
    def set_article
        @article = Article.find(params[:id])
     end 
 end
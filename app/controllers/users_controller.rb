class UsersController < ApplicationController

    def new
        @user = User.new
    end 

    def create
       @user = User.new(user_params)
       if(@user.save)
        flash[:success] = "Welcome to the alpha blog #{@user.name}"
        redirect_to article_url
       else 
            #error
            render 'new'
       end
    end

    def edit
        @user = User.find(params[:id])
       end

       def update
        @user = User.find(params[:id])
        if @user.update(user_params)
         flash[:success] = "Your account was updated successfully"
         redirect_to articles_path
        else
         render 'edit'
        end
       end

       def show
        @user = User.find(params[:id])
       end

    private

    def user_params
        params.require(:user).permit(:name, :email, :password)
    end

end
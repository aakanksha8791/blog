class User < ActiveRecord::Base 

    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

    validates :name, presence: true, uniqueness: true, length: { minimum: 3, maximum: 25 }
    validates :email, presence: true, uniqueness: true, format: { with: VALID_EMAIL_REGEX }
    before_save { self.email = email.downcase }
    has_many :articles
    has_secure_password
    
end
